Role Name
=========

A role that installs and configure a NTP server.

Role Variables
--------------

The most important variables are listed below:

``` yaml
ntp_service_enabled: True
ntp_statistics_enabled: False

ntp_allow_external_clients: False
ntp_allowed_clients: []
#  - { ip: '', netmask: '', options: '' }

ntp_define_servers_pool: False
ntp_servers_pool: []
# - x.y.z.w
# - w.y.z.x
```

Dependencies
------------

None

License
-------

EUPL-1.2

Author Information
------------------

Andrea Dell'Amico, <andrea.dellamico@isti.cnr.it>
